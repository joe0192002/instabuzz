package com.jab.crypto.AlgoTrader.model.instagram;

import javax.persistence.*;

@Table(name="followers")
@Entity
public class FollowersDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "username")
    private String username;

    @Column(name = "dateAdded")
    private long dateAdded;

    @Column(name = "dateRemoved")
    private long dateRemoved;

    public FollowersDAO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(long dateAdded) {
        this.dateAdded = dateAdded;
    }

    public long getDateRemoved() {
        return dateRemoved;
    }

    public void setDateRemoved(long dateRemoved) {
        this.dateRemoved = dateRemoved;
    }
}
