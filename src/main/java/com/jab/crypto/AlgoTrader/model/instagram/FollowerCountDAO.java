package com.jab.crypto.AlgoTrader.model.instagram;

import javax.persistence.*;

@Table(name="follower_count")
@Entity
public class FollowerCountDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "date")
    private long date;

    @Column(name = "count")
    private long count;

    public FollowerCountDAO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
