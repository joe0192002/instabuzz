package com.jab.crypto.AlgoTrader;

import com.jab.crypto.AlgoTrader.model.instagram.FollowerCountDAO;
import com.jab.crypto.AlgoTrader.model.instagram.FollowersDAO;
import com.jab.crypto.AlgoTrader.repository.instagram.FollowerCountRepository;
import com.jab.crypto.AlgoTrader.repository.instagram.FollowersRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;

@Component
public class InstagramRunner {

    private static final Logger logger = LoggerFactory.getLogger(InstagramRunner.class);

    private InstagramManager instagramManager;
    private FollowerCountRepository followerCountRepository;
    private FollowersRepository followersRepository;

    public InstagramRunner(InstagramManager instagramManager,
                           FollowerCountRepository followerCountRepository,
                           FollowersRepository followersRepository) {
        this.instagramManager = instagramManager;
        this.followerCountRepository = followerCountRepository;
        this.followersRepository = followersRepository;
    }

    /**
     * Every 3 hours
     */
    //@Scheduled(fixedRateString = "10800000")
    @PostConstruct
    public void runner() {

        //getAndStoreFollowerData();

        instagramManager.likePostsOfUsersWhoLikeThisPost("https://www.instagram.com/p/CNfLpsNh9lQ/",3,6,200);

    }

    private void getAndStoreFollowerData() {

        List<String> followerNodes = instagramManager.getAllInstagramFollowersForUser("wellbeingbarista");
        logger.info("Retrieved {} followers.",followerNodes.size());

        // save count snapshot
        saveFollowerCountSnapshot(followerNodes.size());

        // persist new followers and remove old ones.
        manageFollowers(followerNodes);
    }

    private void manageFollowers(List<String> followerNodes) {

        // Get DB based follower to mark those who no longer follow us
        List<FollowersDAO> knownFollowers = followersRepository.findAll();
        for (FollowersDAO followersDAO:knownFollowers) {
            if (followersDAO.getDateRemoved() == 0
                    && !isFollowerInRemoteList(followersDAO,followerNodes)) {
                logger.info("Follower {} is no longer following us.",followersDAO.getUsername());
                followersDAO.setDateRemoved(new Date().getTime());
                followersRepository.save(followersDAO);
            }
        }

        // Insert new ones
        for (String node:followerNodes) {
            if (!isFollowerInLocalList(node,knownFollowers)) {

                FollowersDAO followersDAO = new FollowersDAO();
                followersDAO.setUsername(node);
                followersDAO.setDateAdded(new Date().getTime());
                followersDAO.setDateRemoved(0);

                followersRepository.save(followersDAO);
                logger.info("Follower {} is now following us.",followersDAO.getUsername());
            }
        }
    }

    private boolean isFollowerInLocalList(String node, List<FollowersDAO> knownFollowers) {

        for (FollowersDAO followersDAO:knownFollowers) {
            if (followersDAO.getUsername().equals(node)) {
                return true;
            }
        }

        return false;
    }

    private boolean isFollowerInRemoteList(FollowersDAO followersDAO, List<String> followerNodes) {

        for (String node:followerNodes) {
            if (node.equals(followersDAO.getUsername())) {
                return true;
            }
        }

        return false;
    }

    private void saveFollowerCountSnapshot(int size) {

        FollowerCountDAO followerCountDAO = new FollowerCountDAO();
        followerCountDAO.setCount(size);
        followerCountDAO.setDate(new Date().getTime());

        followerCountRepository.save(followerCountDAO);
    }

}
