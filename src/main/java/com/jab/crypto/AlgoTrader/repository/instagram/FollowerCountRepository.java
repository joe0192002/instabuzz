package com.jab.crypto.AlgoTrader.repository.instagram;

import com.jab.crypto.AlgoTrader.model.instagram.FollowerCountDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FollowerCountRepository extends JpaRepository<FollowerCountDAO, Long> {
}
