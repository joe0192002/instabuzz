package com.jab.crypto.AlgoTrader;

import liquibase.pro.packaged.E;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

@Component
public class InstagramManager {

    private static final Logger logger = LoggerFactory.getLogger(InstagramManager.class);
    private final String instagramURL = "https://www.instagram.com/";

    @Value("${instagram.username}")
    private String username;

    @Value("${instagram.password}")
    private String password;

    private boolean loggedIn = false;
    private ChromeDriver chromeDriver;
    @PostConstruct
    public void initAndLogin() {
        chromeDriver = new ChromeDriver();
        chromeDriver.get(instagramURL);

        // Accept cookies
        WebElement acceptCookiesButton = chromeDriver.findElementByXPath("/html/body/div[2]/div/div/div/div[2]/button[1]");
        acceptCookiesButton.click();

        // Wait for login form, Insert username & password & click login
        WebElement usernameInputField =
                (new WebDriverWait(chromeDriver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/section/main/article/div[2]/div[1]/div/form/div/div[1]/div/label/input")));
        usernameInputField.sendKeys(username);
        chromeDriver.findElementByXPath("/html/body/div[1]/section/main/article/div[2]/div[1]/div/form/div/div[2]/div/label/input")
                .sendKeys(password);
        chromeDriver.findElementByXPath("/html/body/div[1]/section/main/article/div[2]/div[1]/div/form/div/div[3]/button/div")
                .click();

        // Save loging info
        WebElement saveLoginInfoButton =
                (new WebDriverWait(chromeDriver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/section/main/div/div/div/section/div/button")));
        saveLoginInfoButton.click();

        // Don't accept notifications
        WebElement notificationsButton =
                (new WebDriverWait(chromeDriver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[4]/div/div/div/div[3]/button[2]")));
        notificationsButton.click();
        loggedIn = true;

    }

    public List<String> getAllInstagramFollowersForUser(String username) {

        List<String> followerUserNames = new ArrayList<>();

        if (loggedIn) {

            chromeDriver.get(instagramURL+username);

            // open followers div
            WebElement followerLink = (new WebDriverWait(chromeDriver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/section/main/div/header/section/ul/li[2]/a")));
            followerLink.click();

            // wait for get scrollable div
            WebElement scrollableDiv =
                    (new WebDriverWait(chromeDriver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[5]/div/div/div[2]/ul/div")));

            // scroll to end
            long lastHeight = (Long)chromeDriver.executeScript("return document.querySelector(\"body > div.RnEpo.Yx5HN > div > div > div.isgrP > ul > div\").scrollHeight");
            boolean stop = false;
            do {
                chromeDriver.executeScript("document.querySelector(\"body > div.RnEpo.Yx5HN > div > div > div.isgrP > ul > div\").scrollIntoView({ behavior: 'smooth', block: 'end' });");
                try {
                    Thread.sleep(2500);
                } catch (InterruptedException e) {
                    logger.error("InterruptedException : ", e);
                }
                long newLastHeight = (Long)chromeDriver.executeScript("return document.querySelector(\"body > div.RnEpo.Yx5HN > div > div > div.isgrP > ul > div\").scrollHeight");
                if (newLastHeight == lastHeight) {
                    stop = true;
                } else {
                    lastHeight = newLastHeight;
                }
            } while (!stop);

            // Get all usernames
            List<WebElement> allFollowers = chromeDriver.findElementsByCssSelector("body > div.RnEpo.Yx5HN > div > div > div.isgrP > ul > div > li > div > div.t2ksc > div.enpQJ > div.d7ByH > span > a");

            // Map
            for (WebElement webElement:allFollowers) {
               followerUserNames.add(webElement.getText());
            }

        }

        return followerUserNames;
    }

    public void likePostsOfUsersWhoLikeThisPost(String postURL, int minimumLikes, int maximumLikes, int maximumUsers) {

        List<String> usersWhoLikeThePost = getUsersLikingPost(postURL,maximumUsers);

        for (String user:usersWhoLikeThePost) {

            //Open their profile
            chromeDriver.get(instagramURL+user);

            // Click on most recent post
            // open likes div
            WebElement firstPostLink = (new WebDriverWait(chromeDriver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/section/main/div/div[3]/article/div[1]/div/div[1]/div[1]/a")));

            // Do we have posts?
            if (firstPostLink != null) {
                firstPostLink.click();

                Random random = new Random();
                int numberOfPostsToLike = random.nextInt(maximumLikes - minimumLikes + 1) + minimumLikes;
                int liked = 0;
                WebElement nextRightButton = null;
                do {
                    // Like it
                    WebElement likeButton = (new WebDriverWait(chromeDriver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[5]/div[2]/div/article/div[3]/section[1]/span[1]/button")));
                    likeButton.click();
                    liked++;

                    // Move onto next
                    try {
                        if (liked == 1) {
                            nextRightButton = (new WebDriverWait(chromeDriver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[5]/div[1]/div/div/a")));
                        } else {
                            nextRightButton = (new WebDriverWait(chromeDriver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[5]/div[1]/div/div/a[2]")));
                        }
                        if (nextRightButton != null) {
                            nextRightButton.click();
                        }
                    } catch (Exception e) {
                        logger.error("Failed getting next right button");
                    }

                } while (liked < numberOfPostsToLike || nextRightButton == null);
            }
        }

    }

    private List<String> getUsersLikingPost(String postURL, int maximumUsers) {

        List<String> followerUserNames = new ArrayList<>();
        if (loggedIn) {

            // open post
            chromeDriver.get(postURL);

            // open likes div
            WebElement likesLink = (new WebDriverWait(chromeDriver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/section/main/div/div[1]/article/div[3]/section[2]/div/div/a")));
            likesLink.click();

            // wait for get scrollable div
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                logger.error("Error in waiting",e);
            }

            // scroll to end
            long lastHeight = (Long)chromeDriver.executeScript("return document.querySelector(\"body > div.RnEpo.Yx5HN > div > div > div.Igw0E.IwRSH.eGOV_.vwCYk.i0EQd > div > div\").scrollHeight");
            boolean stop = false;
            do {
                chromeDriver.executeScript("document.querySelector(\"body > div.RnEpo.Yx5HN > div > div > div.Igw0E.IwRSH.eGOV_.vwCYk.i0EQd > div > div\").scrollIntoView({ behavior: 'smooth', block: 'end' });");
                try {
                    Thread.sleep(2500);
                } catch (InterruptedException e) {
                    logger.error("InterruptedException : ", e);
                }
                long newLastHeight = (Long)chromeDriver.executeScript("return document.querySelector(\"body > div.RnEpo.Yx5HN > div > div > div.Igw0E.IwRSH.eGOV_.vwCYk.i0EQd > div > div\").scrollHeight");
                if (newLastHeight == lastHeight) {
                    stop = true;
                } else {
                    lastHeight = newLastHeight;
                }
                // Get all rendered usernames
                followerUserNames.addAll(
                        convertWebElementsToUsername(
                                chromeDriver.findElementsByCssSelector("body > div.RnEpo.Yx5HN > div > div > div.Igw0E.IwRSH.eGOV_.vwCYk.i0EQd > div > div > div > div > div > div > span > a")));
            } while (!stop && maximumUsers > followerUserNames.size());

        }

        return followerUserNames;
    }

    private Collection<String> convertWebElementsToUsername(List<WebElement> elementsByCssSelector) {
        List<String> results = new ArrayList<>();

        for (WebElement webElement:elementsByCssSelector) {
            results.add(webElement.getText());
        }

        return results;
    }
}
